import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

interface Array
{
  success: boolean,
  data: number[]
}

export interface Evaluacion
{
  nro: number,
  number: number,
  quality: number,
  firstPosition:number,
  lastPosition:number,
  classBg: string,
}

@Component({
  selector: 'app-ejercicio1',
  templateUrl: './ejercicio1.component.html',
  styleUrls: ['./ejercicio1.component.css']
})
export class Ejercicio1Component implements OnInit {

  ///datos que vienen del primer ejercicio
  array$: Array = {success: false, data: []};
  arrayRevesa$: number[] =[];
  ///estructura de elemento encontrado vs cantidad de registro
  arryEvaluado$ = [];
  ///Resultado resuelto, listo p[ara se impreso en pantalla
  lstEvaluacion: Evaluacion[] = [];
  ///valores ordenados
  valores: string = "";
  ///status de conexión
  status: string = "";

  constructor(private http: HttpClient){
  }
  
  ///set array de MiningTag
  ngOnInit() {

  }

  ///Print 
 async get_arrays(){

  this.lstEvaluacion = [];
  this.valores = "";
  this.arryEvaluado$ = [];
  this.array$ = {success: false, data: []};
  this.arrayRevesa$ =[];


     await this.http.get<Array>('http://168.232.165.184/prueba/array').toPromise().then(ar => {
      
    
      if(ar.success)
      {
        this.status = "Ejecución exitosa";
        console.log(this.status);
       
     
        ///datos puros
        console.log(ar.data);
        

        ///Generamos estructura de repetidos
        let repetido = this.getRepetidos(ar.data);
        console.log(repetido);

        this.formatArray(repetido, ar.data);
        console.log(this.lstEvaluacion);

        let arrayNumeros= this.arryEvaluado$.map(function(e)
        {
            return e.value
        });

        this.valores = this.getOrdenados(arrayNumeros).join();
      }
      else
      {
        this.status = 'imposible conectar con minig tag';
        console.error(this.status);
      }
       
    }).catch(e=> {
      this.status = "Error en el sistema: " + e;
      console.error(e);
    }
      );
  }

 


  ///formato al array según lo solicitado en la prueba
  formatArray(array: { value: number, cantidad: number }[], completo)
  {
    for(let i = 0; i < array.length; i++) {

      let registro: Evaluacion = { nro: 0, number: 0, quality: 0, firstPosition:0, lastPosition:0, classBg: ''}
      let item = array[i];
      registro.nro = i + 1;
      registro.number = item.value;
      registro.quality = item.cantidad;
      registro.firstPosition = this.getFirtPostion(item.value, completo);
      registro.lastPosition = item.cantidad === 1 ? registro.firstPosition : this.getLastPosition(item.value, completo);
      registro.classBg = this.getBg(item.cantidad);

      this.lstEvaluacion.push(registro);
    }
  }

  ///filtra los repetidos
  getRepetidos(array)
  {
    
    let largo = array.length;
    
    for (let i=0;i<largo;i++){          
        // i=0
        // value = 11
        let value = array[i];
        let cantidad = 0;
       
        for (let y=0;y<largo;y++){
            // y = 1
            // array[1] = 5 
            if(array[y]===value)
            {
                cantidad +=1;
            }
        }

        if(cantidad > 0 && !this.checkValueExist(value, this.arryEvaluado$))
        {
            let encontrado = {
                value: value,
                cantidad: cantidad
            };
            
            this.arryEvaluado$.push(encontrado);
            
        }        
    }
    
    return this.arryEvaluado$;
  }

  ///verifica exista de un valor en un array
  ///input: 
  ///value: valor buscado
  ///arr: array que se va a comparar
  checkValueExist(valor: number, arr){
    let status = false; 
  
    arr.forEach(function(x, y) {
      
      if(x.value === valor){
        status = true;
      }
    });

    return status;
  }

  ///Retorna la primera posición donde encuentra el registro
  ///input:
  ///value= valor buscado
  getFirtPostion(value: number, completo){
    
    return completo.indexOf(value);
  }

  ///Retorna la última posición donde encuentra el registro
  ///input:
  ///value= valor buscado
  getLastPosition(value:number, completo)
  {

    let posicion = 0;
    completo.forEach(function(x, y) {
 
      if(value === x)
      {
        posicion = y;
      }
    })
    return posicion;
  }

  ///Evalua el color de la fila
  getBg(value:number)
  {
    if(value>=2)
    {
      return 'bg-success';
    }
    else if(value== 1)
    {
      return 'bg-warning';
    }
    else if(value <1)
    {
      return 'bg-danger';
    }
    else{
      return '';
    }
  }

  ///ordena lista de menor a mayor
  ///input:
  ///arrayEnOrden: array a ordenar
  getOrdenados(arrayEnOrden)
  {
    
      let swap:number;
      var swapCount: number;

      do{
          for (var i = 1, swapCount = 0; i < arrayEnOrden.length; i++){
              if(arrayEnOrden[i - 1]>arrayEnOrden[i]){
                  swap = arrayEnOrden[i - 1];
                  arrayEnOrden[i - 1] = arrayEnOrden[i];
                  arrayEnOrden[i] = swap; 
                  swapCount +=1;
              }
          }
      }while(swapCount > 0); 

      return arrayEnOrden; 
  }

}
