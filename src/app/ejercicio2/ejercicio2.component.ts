import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface Paragraph
{
  paragraph: string,
  number: number,
  hasCopyright: boolean,
}

export interface IRemoteResponse {
  data?: any;
  error?: any;
  success: boolean;
}

export interface Evaluacion
{
  nro: number,
  frase: string,
}

export interface Evaluacion2
{
  letra: string,
  number:number,
}

@Component({
  selector: 'app-ejercicio2',
  templateUrl: './ejercicio2.component.html',
  styleUrls: ['./ejercicio2.component.css']
})

export class Ejercicio2Component implements OnInit {
///status de conexión
status: string = "";
para: Paragraph = {paragraph: '', number:0,hasCopyright:false};
retorno: IRemoteResponse;
monto:number = 0;
letrasxCantidad: {} = "";


///Resultado resuelto, listo p[ara se impreso en pantalla
lstEvaluacion: Evaluacion[] = [];
///para el conteo de letras
lst: Evaluacion2[] = [];

  constructor(private http: HttpClient){
  }

  ngOnInit() {

  }

   ///Print 
 async get_para(){


  this.retorno = null;
  this.lstEvaluacion = [];
  this.lst = [];

  await this.http.get<IRemoteResponse>('http://168.232.165.184/prueba/dict').toPromise().then(ar => {
   
  
  //  this.valores = "";
  //  this.arryEvaluado$ = [];
  //  this.array$ = {success: false, data: []};
  //  this.arrayRevesa$ =[];
  this.retorno = ar;
   if(this.retorno.success)
   {
      
      this.retorno.error = "Ejecución exitosa";
      this.status = this.retorno.error ;
     
     console.log(this.retorno);
     

     
      let ordernados = this.getOrdenados(ar.data);
      console.log(ordernados);

      this.getFormat(ordernados);
      console.log(this.lstEvaluacion);

      let array = this.lstEvaluacion.map(function(e)
       {
           return e.frase
       });

      let letraCantidad = this.getCantidadLetras(array);
      this.letrasxCantidad = letraCantidad;
      ///estructura de caracter - cantidad
      console.log(letraCantidad); 

      let suma = this.getSumatoria(this.retorno.data, "number");
      debugger;
      this.monto = suma;
      

   }
   else
   {
      this.retorno.error = 'imposible conectar con minig tag';
      console.error('imposible conectar con minig tag');
   }
    
 }).catch(e=> {
    this.retorno.success = false;
    this.retorno.error = "Error en el sistema: " + e;
    console.error(e);
 }
   );
}

getSumatoria(items, prop){
  return items.reduce( function(a, b){
      return a + b[prop];
  }, 0);
};

  getCantidadLetras(array:string[])
  {
    var o = {};
    var l = [];
    array.forEach(function(w) 
    {
        w.split('').forEach(function(e) {
          let n = (o[e] || 0) + 1
          let evaluacion = {
            letra: e,
            cantidad: n
          } 
          l.push(evaluacion);
          return o[e] = (o[e] || 0) + 1;
        });
 
        
    });
    
    console.log(l);
    return o;
  }

  ///Genera estructura de la primera grilla
  ///input:
  ///array: array de paragraph
  getFormat(array:Paragraph[])
  {


    for(let i = 0; i < array.length; i++) {

      let registro: Evaluacion = { nro: 0, frase: '' };
      let item = array[i];
      registro.nro = i + 1;
      registro.frase = item.paragraph;
    

      this.lstEvaluacion.push(registro);
    }
  }

  getOrdenados(frases:Paragraph[])
  {
    let orden = frases.sort(function(a,b){
      return a.paragraph.localeCompare(b.paragraph);
    });

    return orden;
  }

}
