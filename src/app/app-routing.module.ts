import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Ejercicio1Component } from './ejercicio1/ejercicio1.component'
import { Ejercicio2Component } from './ejercicio2/ejercicio2.component'

const routes: Routes = [
  {path: 'page1', component: Ejercicio1Component},
  {path: 'page2', component: Ejercicio2Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
